package filters

// CompositeFilter chains output internal filter to next internal filter input
type CompositeFilter struct {
	filters []Filter
}

// Filter implements Filter.Filter()
// Filter chains input and output of it's internal filters
func (filter *CompositeFilter) Filter(files <-chan string) <-chan string {
	cout := make(chan string)

	go func() {
		defer close(cout)

		cin := files
		for _, f := range filter.filters {
			cin = f.Filter(cin)
		}

		for file := range cin {
			cout <- file
		}
	}()

	return cout
}

// NewCompositeFilter creates CompositeFilter with specified nested filters
func NewCompositeFilter(filters ...Filter) *CompositeFilter {
	return &CompositeFilter {
		filters: filters,
	}
}