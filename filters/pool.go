package filters

import "bitbucket.org/kyr0s/enconv/utils/async"

// PooledFilter delegates work to the pool of internal converters
type PooledFilter struct {
	filterFactory func() Filter
	bufferSize    int
	poolSize      int
}

// Filter implements Filter.Filter()
// Filter addresses incoming messages to the internal converters pool
func (filter *PooledFilter) Filter(files <-chan string) <-chan string {
	cout := make(chan string, filter.bufferSize)

	worker := func(cin <-chan string) <-chan string {
		c := filter.filterFactory()
		return c.Filter(cin)
	}

	async.PoolString(files, cout, worker, filter.poolSize)
	return cout
}

// NewPooledFilter creates new PooledFilter with specified nested filters factory, pool size and output channel buffer size
func NewPooledFilter(filterFactory func() Filter, poolSize int, bufferSize int) *PooledFilter {
	return &PooledFilter{
		filterFactory: filterFactory,
		poolSize:      poolSize,
		bufferSize:    bufferSize,
	}
}
