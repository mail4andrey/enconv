package filters

import(
	"fmt"
	"strings"

	"github.com/kyr0s/go-gitignore"
)

// GitignoreSettings describes .gitignore-like patterns
// Possible Mode values:
//    "include" - filter returns only that files which satisfy pattern
//    "exclude" - filter returns only that files which are not satisfy pattern
// Panic on any other Mode value
type GitignoreSettings struct {
	Mode string
	Patterns []string
}

func isIncludeMode(mode string) bool {
	return mode == includeMode
}

func isExcludeMode(mode string) bool {
	return mode == excludeMode
}

const includeMode string = "include"
const excludeMode string = "exclude"

// Validate check settings content
func (settings *GitignoreSettings) Validate() {
	if !isExcludeMode(settings.Mode) && !isIncludeMode(settings.Mode) {
		panic(fmt.Sprintf("Unknown mode \"%v\"", settings.Mode))
	}

	if isIncludeMode(settings.Mode) {
		if len(settings.Patterns) == 0 {
			panic(fmt.Sprintf("Rules must be set for \"%v\" mode", includeMode))
		}
	}
}

// GitignoreFilter performs input files filtration based on .gitignore-like patterns
type GitignoreFilter struct {
	gitignore gitignore.IgnoreMatcher
	mode string
}

// Filter implements Filter.Filter()
func (filter *GitignoreFilter) Filter(files <-chan string) <-chan string {
	cout := make(chan string)

	go func() {
		defer close(cout)

		for file := range files {

			pathMatched := filter.gitignore.Match(file, false)
			if pathMatched {
				if isIncludeMode(filter.mode) {
					cout <- file
				}
			} else {
				if isExcludeMode(filter.mode) {
					cout <- file
				}
			}
		}
	}()

	return cout
}

// NewGitIgnoreFilter creates GitignoreFilter with specified base path
func NewGitIgnoreFilter(settings GitignoreSettings, basePath string) *GitignoreFilter {
	ignoreContent := strings.Join(settings.Patterns, "\n")
	ignoreReader := strings.NewReader(ignoreContent)
	gitignore := gitignore.NewGitIgnoreFromReader(basePath, ignoreReader)

	return &GitignoreFilter{
		gitignore: gitignore,
		mode: settings.Mode,
	}
}